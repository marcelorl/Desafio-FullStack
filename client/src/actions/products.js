import axios from '../services/Request';

export const PRODUCTS_FAIL_FETCH = 'PRODUCTS_FAIL_FETCH';
export const PRODUCTS_REQUEST_FETCH = 'PRODUCTS_REQUEST_FETCH';
export const PRODUCTS_SUCCESS_FETCH = 'PRODUCTS_SUCCESS_FETCH';

const requestProductsFail = err =>
  ({
    type: PRODUCTS_FAIL_FETCH,
    err
  });

const requestProducts = () =>
  ({
    type: PRODUCTS_REQUEST_FETCH
  });

const requestProductsSuccess = products =>
  ({
    type: PRODUCTS_SUCCESS_FETCH,
    products
  });

export const fetchProducts = () =>
  dispatch => {
    dispatch(requestProducts());
    return axios.get('/product')
      .then(products => dispatch(requestProductsSuccess(products)))
      .catch(err => dispatch(requestProductsFail(err)));
  };
