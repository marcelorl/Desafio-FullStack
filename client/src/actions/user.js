export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';

const requestLogin = user =>
  ({
    type: USER_LOGIN,
    user
  });

const requestLogout = () =>
  ({
    type: USER_LOGOUT
  });

export const login = user =>
  dispatch => {
    dispatch(requestLogin(user));
  };

export const logout = () =>
  dispatch => {
    dispatch(requestLogout());
  };
