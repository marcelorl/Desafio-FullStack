import axios from '../services/Request';

export const PRODUCT_FAIL_FETCH = 'PRODUCT_FAIL_FETCH';
export const PRODUCT_REQUEST_FETCH = 'PRODUCT_REQUEST_FETCH';
export const PRODUCT_SUCCESS_FETCH = 'PRODUCT_SUCCESS_FETCH';

export const SAVE_FAIL_FETCH = 'SAVE_FAIL_FETCH';
export const SAVE_REQUEST_FETCH = 'SAVE_REQUEST_FETCH';
export const SAVE_SUCCESS_FETCH = 'SAVE_SUCCESS_FETCH';

const requestProductFail = err =>
  ({
    type: PRODUCT_FAIL_FETCH,
    err
  });

const requestProduct = () =>
  ({
    type: PRODUCT_REQUEST_FETCH
  });

const requestProductSuccess = product =>
  ({
    type: PRODUCT_SUCCESS_FETCH,
    product
  });

const saveRequestProductFail = err =>
  ({
    type: SAVE_FAIL_FETCH,
    err
  });

const saveRequestProduct = () =>
  ({
    type: SAVE_REQUEST_FETCH
  });

const saveRequestProductSuccess = () =>
  ({
    type: SAVE_SUCCESS_FETCH
  });

export const fetchProduct = id =>
  dispatch => {
    dispatch(requestProduct());
    return axios.get(`/product/${id}`)
      .then(products => dispatch(requestProductSuccess(products)))
      .catch(err => dispatch(requestProductFail(err)));
  };

export const saveProduct = product =>
  dispatch => {
    dispatch(saveRequestProduct());
    return axios.post('/product', product)
      .then(() => dispatch(saveRequestProductSuccess()))
      .catch(err => dispatch(saveRequestProductFail(err)));
  };
