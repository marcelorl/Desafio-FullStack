import axios from '../services/Request';

export const SHIPPING_FAIL_FETCH = 'SHIPPING_FAIL_FETCH';
export const SHIPPING_REQUEST_FETCH = 'SHIPPING_REQUEST_FETCH';
export const SHIPPING_SUCCESS_FETCH = 'SHIPPING_SUCCESS_FETCH';

const shippingFailAction = err =>
  ({
    type: SHIPPING_FAIL_FETCH,
    err
  });

const shippingAction = () =>
  ({
    type: SHIPPING_REQUEST_FETCH
  });

const shippingSuccessAction = data =>
  ({
    type: SHIPPING_SUCCESS_FETCH,
    data
  });

export const calcShipping = cep =>
  dispatch => {
    dispatch(shippingAction());
    return axios.get('/shipping', { params: { cep } })
      .then(data => dispatch(shippingSuccessAction(data)))
      .catch(err => dispatch(shippingFailAction(err)));
  };
