import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Col,
  Container,
  Row
} from 'reactstrap';

import ProductItem from '../../molecules/ProductItem';
import Slider from '../../molecules/Slider';

class Home extends Component {
  render () {
    const { loading, list } = this.props.product;

    if (loading) { return null; }

    return (
      <Container className='mb-4'>
        <Row>
          <Col>
            <Slider key='slider' />
          </Col>
        </Row>
        <Row className='mt-3'>
          {list.map((product, key) =>
            <Col key={key} lg='3'>
              <ProductItem product={product} />
            </Col>
          )}
        </Row>
      </Container>
    );
  }
}

Home.propTypes = {
  products: PropTypes.array
};

export default Home;
