import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'reactstrap';
import get from 'lodash.get';

class Thankyou extends Component {
  renderLinkToBoleto () {
    const status = get(this, 'props.checkout.status', {});
    const { boleto_url, payment_method } = status;

    if (payment_method === 'boleto') {
      return (
        <div>
          <a className="mb-4" href={boleto_url}>Clique aqui para pagar seu boleto</a>
        </div>
      );
    }
  }

  render () {
    return (
      <Container>
        <div className="mb-4">Obrigado por comprar em nossa loja.</div>

        {this.renderLinkToBoleto()}

        <Link to="/">Página Principal</Link>
      </Container>
    );
  }
}

export default Thankyou;
