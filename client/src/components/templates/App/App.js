import React, { Component } from 'react';
import { Container } from 'reactstrap';

import Header from '../../organisms/Header';

import './App.css';

class App extends Component {
  render () {
    return (
      <Container>
        <Header
          auth={this.props.auth}
          onLogout={this.props.logout}
          user={this.props.user}
          cartQuantity={this.props.cart.products.length} />
        {this.props.children}
      </Container>
    );
  }
}

export default App;
