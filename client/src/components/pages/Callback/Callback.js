import React from 'react';

import CallbackTemplate from '../../templates/Callback';

const Callback = () =>
  <CallbackTemplate {...this.props} />;

export default Callback;
