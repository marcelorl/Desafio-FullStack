# Desafio FullStack Developer - Marcelo Rizzardo Lima

### Features
 - Docker
 - React 16
 - Redux
 - glamorous
 - Auth0
 - Reactstrap
 - Atomic Design (Client-side)
 - Express server
 - node-correios
 - sequelize
 - DDD (Server-side)
 - Semistandard (linting)
 - editorconfig
 - [Commit amigão](https://github.com/BeeTech-global/bee-stylish/tree/master/commits)

### Instalação

 - Instale a ultima versão do docker na sua maquina
 - Rode na pasta root do projeto: `docker-compose up`
 
 Pronto!
 
### Uso

 - Para acessar o site:
  http://localhost:3000
  http://localhost:3000/admin -> para cadastrar produtos na loja
  
 - Server 
  http://localhost:1337

### Build

  `cd client/ && yarn build`
